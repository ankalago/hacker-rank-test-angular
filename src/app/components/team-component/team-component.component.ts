import { Component, OnInit, Input } from '@angular/core';
import { Team } from '../team-list/team-list.component';

@Component({
  selector: 'app-team-component',
  templateUrl: './team-component.component.html',
  styleUrls: ['./team-component.component.css']
})
export class TeamComponent implements OnInit {
  // IMP - Implement addChannel and removeChannel operations within this component
  @Input() team: Team;
  @Input() teamIndex: number;
  currentIndex: number;
  currentState: number;
  channelName: '';
  currentSort = 'nameAscending';

  constructor() { 
    
  }

  ngOnInit() {
    
  }

  formValidation() {
    const validation = /^[1-9]\d*$/
    return validation.test(this.channelName)
  }

  removeChannel(index) {
    this.team.channels.splice(index, 1)
  }

  addChannel() {
    const newChannel = {
      name: this.channelName,
      index: this.team.channels.length + 1
    }
    this.team.channels.push(newChannel)
    this.channelName = ''
  }

  sort() {
    switch (this.currentSort) {
      case 'nameAscending':
        this.currentSort = 'nameDescending';
        return this.team.channels.sort((a, b) => {
          return a.name > b.name ? 1 : -1;
        })
      case 'nameDescending':
        this.currentSort = 'order'
        return this.team.channels.sort((a, b) => {
          return a.name < b.name ? 1 : -1;
        })
      case 'order':
        this.currentSort = 'nameAscending'
        return this.team.channels.sort((a, b) => {
          return a.index - b.index;
        })
    }
  }
}
